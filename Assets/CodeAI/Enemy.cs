using Pathfinding;
using UnityEngine;

namespace CodeAI
{
    public class Enemy : MonoBehaviour
    {
        public Transform target;

        public float speed;

        public float nextwaypointDistance;

        public Transform enemyGFX;

        public Vector2 direction;

        public Vector2 force;

        public float distance;

        //public currentwaypoint;

        //public nextwaypointDistance;
         
        Path path;
        int currentwaypoint = 0;
        bool reachedEndofPath = false;
        
        Seeker seeker;
        Rigidbody2D rb;
        // Start is called before the first frame update
        void Start()
        {
            seeker = GetComponent<Seeker>();
            rb = GetComponent<Rigidbody2D>();
            
            //seeker.StartPath(rb.position, target.position, OnPathComplete);
            InvokeRepeating("UpdatePath",0f,0.5f);
        }

        // ReSharper disable Unity.PerformanceAnalysis
        void UpdatePath()
        {
            if (seeker.IsDone()) 
                seeker.StartPath(rb.position, target.position, OnPathComplete);
        }

        void OnPathComplete(Path p)
        {
            if (!p.error)
            {
                path = p;
                currentwaypoint = 0;
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (path == null)
                return;

            if (currentwaypoint >= path.vectorPath.Count)
            {
                reachedEndofPath = true;
               return;
            }
            else
            {
                reachedEndofPath = false;
            }

            direction = (Vector2) path.vectorPath[currentwaypoint] - rb.position.normalized;
            force = direction * speed * Time.deltaTime;
            
            rb.AddForce(force);

            distance = Vector2.Distance(rb.position, path.vectorPath[currentwaypoint]);

            if (distance <= nextwaypointDistance)
            {
                currentwaypoint++;
            }
            else
            {
                currentwaypoint--;
            }

            if (force.x >= 0.01f)
            {
                 
                enemyGFX.localScale = new Vector3(-1f, 1f, 1f);

            }
            else if (force.x <= -0.01f)
            {
                enemyGFX.localScale = new Vector3(1f, 1f, 1f);

            }
        }
    }
}
