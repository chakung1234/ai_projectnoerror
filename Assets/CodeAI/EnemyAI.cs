using System;
using System.Text;
using Pathfinding;
using UnityEngine;

namespace CodeAI
{
    public class EnemyAI : MonoBehaviour
    {
        [Header("Pathfindging")] public Transform target;

        public float activateDistance = 50f;
        public float pathUpdateSecond = 0.5f;

        [Header("Physics")] public float speed = 200f;
        public float nextWaypointDistance = 3f;
        public float jumpNodeHeightRequirement = 0.8f;
        public float jumpModifier = 0.3f;
        public float jumpCheckoffset = 0.1f;

        [Header("Custom Behavior")] public bool followEnabled = true;
        public bool jumpEnabled = true;
        public bool directiononlookEnable = true;

        private Path path;
        private int currenWaypoint = 0;
        private bool isGround = false;
        Seeker seeker;
        Rigidbody2D rb;

        public void Start()
        {
            seeker = GetComponent<Seeker>();
            rb = GetComponent<Rigidbody2D>();

            InvokeRepeating("UpdatePath", 0f, pathUpdateSecond);
        }

        private void FixedUpdate()
        {
            if (TargetInDistance() && followEnabled)
            {
                PathFollow();
            }
        }

        // ReSharper disable Unity.PerformanceAnalysis
        private void UpdatePath()
        {
            if (followEnabled && TargetInDistance() && seeker.IsDone())
            {
                seeker.StartPath(rb.position, target.position, OnpathComplete);
            }
        }

        private void PathFollow()
        {
            if (path == null)
            {
                return;

            }

            if (currenWaypoint >= path.vectorPath.Count)
            {
                return;
            }

            isGround = Physics2D.Raycast(transform.position, -Vector3.up, GetComponent<Collider2D>().bounds.extents.y + jumpCheckoffset);

            Vector2 direction = ((Vector2)path.vectorPath[currenWaypoint] - rb.position).normalized;
            Vector2 force = direction * speed * Time.deltaTime;

            if (jumpEnabled && isGround)
            {
                if (direction.y > jumpNodeHeightRequirement)
                {
                    rb.AddForce(Vector2.up * speed * jumpModifier);
                }
            }

            rb.AddForce(force);

            float distance = Vector2.Distance(rb.position, path.vectorPath[currenWaypoint]);
            if (distance < nextWaypointDistance)
            {
                currenWaypoint++;
            }

            if (rb.velocity.x > 0.05f)
            {

                transform.localScale = new Vector3(-1f * Mathf.Abs(transform.localScale.x), transform.localScale.y,
                    transform.localScale.z);

            }
            else if (rb.velocity.x < -0.05f)
            {
                transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y,
                    transform.localScale.z);

            }

        }


        private bool TargetInDistance()
        {
            return Vector2.Distance(transform.position, target.transform.position) < activateDistance;
        }

        private void OnpathComplete(Path p)
        {
            if (!p.error)
            {
                path = p;
                currenWaypoint = 0;
            }
        }
    }
}


    

