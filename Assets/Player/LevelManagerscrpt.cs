using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManagerscrpt : MonoBehaviour
{
    public static LevelManagerscrpt instance;

    public Transform respawnPoint;
    public GameObject playerPrefab;

    private void Awake()
    {
        instance = this;
    }

    public void Respawn()
    {
        Instantiate(playerPrefab, respawnPoint.position, Quaternion.identity);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
