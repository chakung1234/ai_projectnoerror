using System;
using System.Collections;
using System.Collections.Generic;
using CodeAI;
using Lab9.Menu_Code;
using Resources;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerDead : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            SoundManagerscript.Playsound("Bat");
            Destroy(gameObject);
            LevelManagerscrpt.instance.Respawn();
            SceneManager.LoadScene("Gameover", LoadSceneMode.Additive);
            GameApplicationManager.Instance.IsOptionMenuActive = true;
            SoundManagerscript.Playsound("Lose");
            
        }
        if (collision.gameObject.CompareTag("Flag"))
        {
            SoundManagerscript.Playsound("Win");
            Destroy(gameObject);
            LevelManagerscrpt.instance.Respawn();
            SceneManager.LoadScene("Winner", LoadSceneMode.Additive);
            GameApplicationManager.Instance.IsOptionMenuActive = true;
            
        }
        
            }
}


