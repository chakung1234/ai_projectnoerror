using System;
using Resources;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    //public CharacterController2D Controller;

    public float runSpeed = 40f;
    public float JumpForce = 40f;

    private Rigidbody2D _rigidbody;
    //float _horizontalMove = 0f;
    //float _JumpMove = 0f;

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //_horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
        var movement = Input.GetAxis("Horizontal");
        transform.position += new Vector3(movement, 0, 0) * Time.deltaTime * runSpeed;
        //_JumpMove = Input.GetAxisRaw("Vertical") * runSpeed;

        if (Input.GetButtonDown("Jump")&& Mathf.Abs(_rigidbody.velocity.y) < 0.001f)
        {
           
            _rigidbody.AddForce(new Vector2(0,JumpForce),ForceMode2D.Impulse);
            SoundManagerscript.Playsound("Jump");
            //_JumplMove = Input.GetAxisRaw("Vertical") * runSpeed;
        }

    }

    //void FixedUpdate()
    //{
    //    Controller.Move(_horizontalMove * Time.fixedDeltaTime, false, _jump);
        //_jump = false;
    //}
}