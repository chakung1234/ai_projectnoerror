using System;
using UnityEngine;

public class Player : MonoBehaviour
{
    public CharacterController2D Controller;

    public float runSpeed = 40f;
            
    float _horizontalMove = 0f;

    bool _jump = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

        if (Input.GetButtonDown("Jump"))
        {
            _jump = true;
        }

    }

    void FixedUpdate()
    {
        Controller.Move(_horizontalMove * Time.fixedDeltaTime, false, _jump);
        _jump = false;
    }
}