using Lab9.Menu_Code;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Lab9.MainMenuControlScript
{
    public class MainMenuControlScript : MonoBehaviour
    {
        [SerializeField] Button startButton;
        [SerializeField] Button optionsButton;
        [SerializeField] Button exitButton;

        //[SerializeField] Button creditbutton;
        // Start is called before the first frame update
        void Start()
        {
            startButton.onClick.AddListener(
                delegate { StartButtonClick(startButton); });
            optionsButton.onClick.AddListener(
                delegate { OptionButtonClick(optionsButton); });
            exitButton.onClick.AddListener(
                delegate { ExitBottonClick(exitButton); });
          
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        
        
        public void StartButtonClick(Button botton)
        {
            SceneManager.LoadScene("Map");
        }

        public void OptionButtonClick(Button button)
        {
            if (!GameApplicationManager.Instance.IsOptionMenuActive)
            {
                SceneManager.LoadScene("SceneOption", LoadSceneMode.Additive);
                GameApplicationManager.Instance.IsOptionMenuActive = true;
            }
        }

        public void ExitBottonClick(Button botton)
        {
            Application.Quit();
        }
    }
}