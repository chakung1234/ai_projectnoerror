using Lab9.Menu_Code;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Lab9.MainMenuControlScript
{
    public class OptionsMenuControlScript : MonoBehaviour
    {

        //[SerializeField] private Dropdown _dropdownDifficulty;

        [SerializeField] private Toggle _toggleMusic;

        [SerializeField] private Toggle _toggleSFX;

        [SerializeField] private Button _backButton;

        // Start is called before the first frame update
        void Start()
        {
            //_dropdownDifficulty.value = GameApplicationManager.Instance.DifficultyLevel;
            _toggleMusic.isOn = GameApplicationManager.Instance.MusicEnabled;
            _toggleSFX.isOn = GameApplicationManager.Instance.SFXEnabled;

            //_dropdownDifficulty.onValueChanged.AddListener(delegate { DropdownDifficultyChange(_dropdownDifficulty); });

            _toggleMusic.onValueChanged.AddListener(delegate { OnToggleMusic(_toggleMusic);});

            _toggleSFX.onValueChanged.AddListener(delegate { OnToggleSFX(_toggleSFX);});

            _backButton.onClick.AddListener(delegate { BackButtonClick(_backButton);});
        }

        // Update is called once per frame
        void Update()
        {



        }

        public void BackButtonClick(Button button)
        {
            SceneManager.UnloadSceneAsync("SceneOption");
            GameApplicationManager.Instance.IsOptionMenuActive = false;
        }

        //public void DropdownDifficultyChange(Dropdown dropdown)
        //{
        //    GameApplicationManager.Instance.DifficultyLevel = dropdown.value;
        //}

        public void OnToggleMusic(Toggle toggle)
        {
            GameApplicationManager.Instance.MusicEnabled = _toggleMusic.isOn;
        }

        public void OnToggleSFX(Toggle toggle)
        {
            GameApplicationManager.Instance.SFXEnabled = _toggleSFX.isOn;
        }
    }
}