using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OverBotton : MonoBehaviour
{
    [SerializeField] Button Restartbotton;
    [SerializeField] Button Mainmenubotton;
    
    
    void Start()
    {
        Restartbotton.onClick.AddListener(
            delegate { RestartbottonClick(Restartbotton); });
        Mainmenubotton.onClick.AddListener(
            delegate { MainmenubottonClick(Mainmenubotton); });
    }
    public void RestartbottonClick(Button botton)
    {
        SceneManager.LoadScene("Map");
    }

    public void MainmenubottonClick(Button botton)
    { 
        SceneManager.LoadScene("Mainmenu");
    }
}

