using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace Lab9
{
    public class SingletonGameManager : Singleton<SingletonGameManager>
    {
        protected SingletonGameManager(){}
        public string ClassName{get;} = "SingletonGameManager";
        public int GameScore{get;set;} = 0;

        public string textNamePlayer = "Player";
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}