using UnityEngine;

namespace Resources
{
    public class SoundManagerscript : MonoBehaviour
    {
        public static AudioClip win, lose, jump, dead, bat;

        private static AudioSource _audioSource;
        // Start is called before the first frame update
        void Start()
        {
            win = UnityEngine.Resources.Load<AudioClip>("Win");
            lose = UnityEngine.Resources.Load<AudioClip>("Lose");
            jump = UnityEngine.Resources.Load<AudioClip>("Jump");
            dead = UnityEngine.Resources.Load<AudioClip>("Dead");
            bat = UnityEngine.Resources.Load<AudioClip>("Bat");

            _audioSource = GetComponent<AudioSource>();
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        public static void Playsound(string clip)
        {
            switch (clip)
            {
                case "win":
                    _audioSource.PlayOneShot(win);
                    break;
                case "lose":
                    _audioSource.PlayOneShot(lose);
                    break;
                case "jump":
                    _audioSource.PlayOneShot(jump);
                    break;
                case "dead":
                    _audioSource.PlayOneShot(dead);
                    break;
                case "bat":
                    _audioSource.PlayOneShot(bat);
                    break;
            
            }
        }
    }
}
